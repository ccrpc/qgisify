from qgis.core import QgsSingleSymbolRenderer, QgsRendererCategory, \
    QgsCategorizedSymbolRenderer, QgsGraduatedSymbolRenderer, QgsRendererRange
from symbol import Symbol_Converter


class RendererConverter:
    def __init__(self, renderer_type, renderer_def, symbology_def):
        self.renderer_type = renderer_type
        self.renderer_def = renderer_def
        self.symbology_def = symbology_def

    def transparency(self):
        return float(1 - self.symbology_def['transparency']/100)

    def _create_simple(self):
        symbol_converter = Symbol_Converter(
            self.renderer_def['symbol']['type'],
            self.renderer_def['symbol']
        )
        symbol = symbol_converter.convert()
        # set symbol's opacity
        symbol.setOpacity(self.transparency())
        return QgsSingleSymbolRenderer(symbol)

    def _create_categorized(self):
        categories = []
        for value in self.renderer_def['uniqueValueInfos']:
            class_value = value['value']
            label_text = str(value['label'])
            symbol_converter = Symbol_Converter(
                value['symbol']['type'],
                value['symbol'])
            symbol = symbol_converter.convert()
            # set symbol's Opacity
            symbol.setOpacity(self.transparency())
            # set Render Category
            category = QgsRendererCategory(class_value, symbol, label_text)
            categories.append(category)
        return QgsCategorizedSymbolRenderer(
            self.renderer_def['field1'], categories)

    def _create_graduated(self):
        break_list = []
        for value in self.renderer_def['classBreakInfos']:
            symbol_converer = Symbol_Converter(
                value['symbol']['type'],
                value['symbol'])
            symbol = symbol_converer.convert()
            # set symbol's opacity
            symbol.setOpacity(self.transparency())
            min_value = float(value['classMinValue'])
            max_value = float(value['classMaxValue'])
            label_text = str(value['label'])
            # set Render Range
            range = QgsRendererRange(min_value, max_value, symbol, label_text)
            break_list.append(range)
        return QgsGraduatedSymbolRenderer(
            self.renderer_def['field'], break_list)

    def convert(self):
        # simple layer
        if self.renderer_type == 'OTHER':
            return self._create_simple()
        # categories layer
        if self.renderer_type == 'UNIQUE_VALUES':
            return self._create_categorized()
        # graduated color
        if self.renderer_type == 'GRADUATED_COLORS':
            return self._create_graduated()
