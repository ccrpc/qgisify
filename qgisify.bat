@echo off
setlocal ENABLEDELAYEDEXPANSION
set /a success_count=0
set /a failure_count=0
set script_dir=%~dp0

if "%~1"=="" (
    echo No input files specified.
    echo To use the MXD to QGS converter, drag and drop MXD files on top of its icon.
    goto end
)

:fileloop
if "%~1"=="" goto results

set json_path="%tmp%\qgisify-%RANDOM%.json"
set target_path="%~d1%~p1%~n1.qgs"

:checktarget
if exist %target_path% (
    echo Target file already exists: %target_path%
    echo Remove or rename this file before continuing.
    pause
    goto checktarget
)

echo Converting %1...
C:\Python27\ArcGIS10.2\python.exe %script_dir%\mxd2json.py %1 %json_path%
cmd /c C:\OSGeo4W64\bin\python-qgis.bat %script_dir%\json2qgs.py %json_path% %target_path%
if exist %json_path% del %json_path%

if exist %target_path% (
    echo Conversion succeeded.
    set /a success_count = %success_count% + 1
) else (
    echo Conversion failed.
    set /a failure_count = %failure_count% + 1
)
echo.

shift
goto fileloop

:results
echo Conversion results: %success_count% succeeded, %failure_count% failed

:end
pause
