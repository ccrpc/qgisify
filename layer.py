from qgis.core import QgsVectorLayer
from renderer import RendererConverter


class LayerConverter:
    def __init__(self, layer_def, root):
        self.layer_def = layer_def
        self.root = root

    def _create_layer(self):
        src = self.layer_def['dataSource'].replace('\\', '/')
        if '.gdb/' in src:
            src = '.gdb|layername='.join(src.rsplit('.gdb/', 1))
        return QgsVectorLayer(src, self.layer_def['name'], 'ogr')

    def convert(self):
        layer = self._create_layer()
        # define layer's render
        renderer_converter = RendererConverter(
            self.layer_def['symbologyType'],
            self.layer_def['symbology']['renderer'],
            self.layer_def['symbology'])
        renderer = renderer_converter.convert()
        layer.setRenderer(renderer)
        return layer
