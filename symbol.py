from qgis.core import QgsMarkerSymbol, QgsLineSymbol, QgsFillSymbol


def format_color(color):
    return ','.join([str(i) for i in color])


class Symbol_Converter:
    def __init__(self, symbol_type, symbol_def):
        self.symbol_type = symbol_type
        self.symbol_def = symbol_def

    def _create_point(self):
        stroke = '110,110,110,255'
        width = '0'
        if 'outline' in self.symbol_def.keys():
            stroke = format_color(self.symbol_def['outline']['color'])
            width = str(self.symbol_def['outline']['width'])
        # change point color to qgis_format
        return QgsMarkerSymbol.createSimple({
            'name': self.symbol_def['style'][7:].lower(),
            'color': format_color(self.symbol_def['color']),
            'outline_color': stroke,
            'outline_width': width,
            'size': str(self.symbol_def['size']),
            'size_unit': 'Points',
            'outline_width_unit': 'Points',
            'outline_style': 'solid'
        })

    def _create_line(self):
        # change line color to qgis_format
        return QgsLineSymbol.createSimple({
            'line_color': format_color(self.symbol_def['color']),
            'line_width': str(self.symbol_def['width']),
            'line_width_unit': 'Points',
            'outline_style': self.symbol_def['style'][7:].lower()
        })

    def _create_polygon(self):
        stroke = '110,110,110,255'
        outline_style = 'solid'
        width = '0'
        if 'outline' in self.symbol_def.keys():
            stroke = format_color(self.symbol_def['outline']['color'])
            width = str(self.symbol_def['outline']['width'])
            outline_style = self.symbol_def['outline']['style'][7:].lower()
        # change polygon color to qgis_format
        return QgsFillSymbol.createSimple({
            'color': format_color(self.symbol_def['color']),
            'outline_color': stroke,
            'outline_width': width or '0',
            'outline_width_unit': 'Points',
            'outline_style': outline_style
        })

    def convert(self):
        # point
        if self.symbol_type == 'esriSMS':
            symbol = self._create_point()
        # polyline
        if self.symbol_type == 'esriSLS':
            symbol = self._create_line()
        # polygon
        if self.symbol_type == 'esriSFS':
            symbol = self._create_polygon()
        return symbol
