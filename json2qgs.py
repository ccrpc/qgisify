import json
import sys
from qgis.core import QgsApplication, QgsProject, \
    QgsRasterLayer, QgsCoordinateReferenceSystem, QgsRectangle
from qgis.gui import QgsMapCanvas
from layer import LayerConverter
from config import SERVICE_LAYER_MAP


class QGSConverter:
    def __init__(self, json_path, qgs_path, **kwargs):
        self.json_path = json_path
        self.qgs_path = qgs_path
        with open(self.json_path, 'r') as json_file:
            self.json = json.load(json_file)
        self.project = QgsProject.instance()
        # The canvas needs to stay in scope so that the extent will be
        # written to the project.
        self.canvas = QgsMapCanvas()
        self.root = self.project.layerTreeRoot()
        self.groups = {}
        self.excluded_groups = []
        self.flatten_service_groups = \
            kwargs.get('flatten_service_groups', True)
        self.service_layer_map = kwargs.get('service_layer_map', {})

    def convert(self):
        for i, df in enumerate(self.json['dataFrames']):
            # TODO: Handle multiple data frames more gracefully.
            # For now we use the CRS and extent from the first one.
            if i == 0:
                self.set_map_crs(df)
                self.set_map_extent(df)
            for layer_def in df['layers']:
                if not layer_def['broken']:
                    self.add_layer(layer_def)

        self.write()

    def set_map_crs(self, df):
        self.project.setCrs(QgsCoordinateReferenceSystem(df['srs']))

    def set_map_extent(self, df):
        self.canvas.setExtent(QgsRectangle(*df['extent']))

    def add_layer(self, layer_def):
        parent = self.root
        if '\\' in layer_def['longName']:
            group = layer_def['longName'].rsplit('\\', 1)[0]
            if group in self.excluded_groups:
                self.excluded_groups.append(layer_def['longName'])
                print('Skipped: %s' % (layer_def['name']))
                return
            parent = self.groups.get(group, self.root)

        if layer_def['serviceProperties'] is None:
            if layer_def['type'] == 'group':
                self.add_group_layer(layer_def, parent)
            if layer_def['type'] == 'feature':
                self.add_vector_layer(layer_def, parent)

        else:
            self.add_service_layer(layer_def, parent)
            if self.flatten_service_groups:
                self.excluded_groups.append(layer_def['longName'])

    def add_group_layer(self, layer_def, parent):
        self.groups[layer_def['longName']] = parent.addGroup(layer_def['name'])
        print('Added group layer: %s' % (layer_def['name'],))

    def add_vector_layer(self, layer_def, parent):
        layer_converter = LayerConverter(layer_def, self.root)
        layer = layer_converter.convert()
        self.add_to_parent(layer, parent)
        self.setVisibility(layer, parent, layer_def)
        self.setScale(layer, layer_def)
        print('Added vector layer: %s' % (layer_def['name'],))

    def add_service_layer(self, layer_def, parent):
        sl = self.service_layer_map.get(layer_def['description'], None)
        if sl:
            layer = QgsRasterLayer(sl['url'], layer_def['name'], sl['type'])
            self.add_to_parent(layer, parent)
            print('Added service layer: %s' % (layer_def['name']))
        else:
            print('Skipped: %s' % (layer_def['name']))

    def add_to_parent(self, layer, parent):
        self.project.addMapLayer(layer, False)
        parent.addLayer(layer)

    def setVisibility(self, layer, parent, layer_def):
        parent.findLayer(layer.id()).setItemVisibilityChecked(
            layer_def['visible']
        )

    def setScale(self, layer, layer_def):
        layer.setMaximumScale(layer_def['maxScale'])
        layer.setMinimumScale(layer_def['minScale'])

    def write(self):
        self.project.write(self.qgs_path)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Input and output filenames are required')
    else:
        qgs = QgsApplication([], False)
        qgs.initQgis()
        converter = QGSConverter(sys.argv[1], sys.argv[2],
                                 service_layer_map=SERVICE_LAYER_MAP)
        converter.convert()
        # The QgsApplication instance needs to be deleted, or else it will
        # throw errors if there are still threads active.
        del qgs
