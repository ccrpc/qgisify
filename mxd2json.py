import arcpy
import json
import sys


def safegetattr(item, attr):
    if hasattr(item, attr):
        return getattr(item, attr)
    return None


class MXDConverter:
    def __init__(self, mxd_path, json_path):
        self.mxd_path = mxd_path
        self.json_path = json_path
        self.map = arcpy.mapping.MapDocument(self.mxd_path)
        self.output = {
            'dataFrames': []
        }

    def df(self, map_df):
        ll = map_df.extent.lowerLeft
        ur = map_df.extent.upperRight

        return {
            'srs': map_df.spatialReference.factoryCode,
            'extent': [ll.X, ll.Y, ur.X, ur.Y],
            'layers': []
        }

    def layer(self, map_layer):
        return {
            'broken': safegetattr(map_layer, 'isBroken'),
            'dataSource': safegetattr(map_layer, 'dataSource'),
            'definitionQuery': safegetattr(map_layer, 'definitionQuery'),
            'description': safegetattr(map_layer, 'description'),
            'transparency': safegetattr(map_layer, 'transparency'),
            'visible': safegetattr(map_layer, 'visible'),
            'name': safegetattr(map_layer, 'name'),
            'longName': safegetattr(map_layer, 'longName'),
            'maxScale': safegetattr(map_layer, 'maxScale'),
            'minScale': safegetattr(map_layer, 'minScale'),
            'type': self.layer_type(map_layer),
            'serviceProperties': safegetattr(map_layer, 'serviceProperties'),
            'symbologyType': safegetattr(map_layer, 'symbologyType'),
            'symbology': json.loads(
                map_layer._arc_object.getsymbology() or 'null')
        }

    def layer_type(self, map_layer):
        if safegetattr(map_layer, 'isFeatureLayer'):
            return 'feature'
        if safegetattr(map_layer, 'isGroupLayer'):
            return 'group'
        if safegetattr(map_layer, 'isNetworkAnalystLayer'):
            return 'networkAnalyst'
        if safegetattr(map_layer, 'isRasterLayer'):
            return 'raster'
        if safegetattr(map_layer, 'isServiceLayer'):
            return 'service'
        return 'other'

    def convert(self):
        for map_df in arcpy.mapping.ListDataFrames(self.map):
            df = self.df(map_df)
            for map_layer in arcpy.mapping.ListLayers(self.map, '', map_df):
                layer = self.layer(map_layer)
                df['layers'].append(layer)
            self.output['dataFrames'].append(df)
        self.write()

    def write(self):
        with open(self.json_path, 'w') as json_file:
            json_file.write(json.dumps(self.output))


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Input and output filenames are required')
    else:
        converter = MXDConverter(sys.argv[1], sys.argv[2])
        converter.convert()
