# Qgisify
Convert map documents from ArcGIS to QGIS

## Usage
In Windows Explorer, drag and drop MXD files on top of the `qgisify.bat` icon.

## License
Qgisify is available under the terms of the [BSD 3-clause
license](https://gitlab.com/ccrpc/qgisify/blob/master/LICENSE.md).
